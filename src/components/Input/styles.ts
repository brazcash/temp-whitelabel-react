import styled from 'styled-components';

export const Container = styled.input`
  font-size: 14px;
  color: #000;
  background: #fff;
  height: 40px;
  border: none;
  border-radius: 15px;
  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
  font-weight: 600;
  margin: 10px 0;
  padding: 26px 22px;
  width: 100%;
  outline: none;
`;
