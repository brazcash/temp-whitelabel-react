import styled , { css } from 'styled-components';

export const Container = styled.button`
  background: ${props => props.theme.colors.primary};
  border: none;
  border-radius: 10px;
  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
  color: #fff;
  font-size: 12px;
  font-weight: bold;
  margin: 2px;
  min-width: 134px;
  outline: none;
  text-transform: uppercase;
  /* Style sub-classes */
  &.light {
    background-color: ${props => props.theme.colors.light};
    color: ${props => props.theme.colors.lightText};
  }
  &.md {
    padding: 16px 22px;
  }
  &.sm {
    padding: 10px 22px;
  }
  &:hover {
    cursor: pointer;
  }
`;
