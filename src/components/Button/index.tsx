import React from 'react';

import { Container } from './styles';

interface Props {
  children?: React.ReactNode;
  className?: string;
}

const Button: React.FC<Props> = ({children, className}) => {
  return (
    <Container className={className}>{children}</Container>
  );
};

export default Button;
