import styled from "styled-components"

export const HeaderContainer = styled.header`
  align-items: center;
  display: flex;
  justify-content: space-between;
  margin: 0 auto;
  max-width: 1920px;
  padding: 30px 50px;
`
export const InputSearchContainer = styled.div`
  border-radius: 100px;
  position: relative;
  width: 393px;
`

export const InputSearch = styled.input`
  background-color: #ECECEC;
  border-radius: 100px;
  border: none;
  border: 4px solid #f6f6f6;
  display: block;
  padding: 20px 60px 20px 30px;
  width: 100%;
  &:focus {
    outline: none;
  }
`

export const InputSearchButton = styled.button`
  background: transparent;
  border: none;
  cursor: pointer;
  display: inline-block;
  font-size: 20px;
  position: absolute;
  top: 0;
  right: 0;
  padding: 18px 30px;
  z-index: 2;
`

export const ButtonsContainer = styled.div`
  display: flex;
  justify-content: space-between;
  width: 300px;
`