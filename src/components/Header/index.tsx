import React from 'react'
import Image from 'next/image'
import Button from '../Button'
import {
  HeaderContainer,
  InputSearchContainer,
  InputSearch,
  InputSearchButton,
  ButtonsContainer,
} from './styles'

interface Props {
  isAuthenticated?: boolean;
}

const Header: React.FC<Props> = ({ isAuthenticated }) => {
  return (
    <HeaderContainer>
      <Image
        src="/img/cashbanx-logo.png"
        alt="logo cashbanx"
        width={399}
        height={91}
        
      />
      <InputSearchContainer>
        <InputSearch placeholder="Buscar loja"/>
        <InputSearchButton>
        <Image
          src="/img/search-icon.png"
          alt="logo  cashbanx"
          width={16}
          height={16}
      />
        </InputSearchButton>
      </InputSearchContainer>
      {
        !isAuthenticated &&
        <ButtonsContainer>
          <Button className="md">
            Cadastrar
          </Button>
          <Button className="md">
            Login
          </Button>
        </ButtonsContainer>
      }
    </HeaderContainer>
  )
}

export default Header;
