import styled from 'styled-components';

export const Container = styled.div`
  width: 633px;
  min-height: 300px;
  margin: 150px 0;
  padding: 14px;
  background: #F7F7F7;
  box-shadow: 0px 0px 20px rgba(0, 0, 0, 0.25);
  border-radius: 25px;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
`;
