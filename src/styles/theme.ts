const theme = {
  colors: {
    background: '#fff',
    light: '#fff',
    lightText: '#000',
    primary: '#38AD3B',
    text: '#e1e1e6',
  }
}

export default theme
