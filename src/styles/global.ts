import { createGlobalStyle } from "styled-components";

export default createGlobalStyle `
  * {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
  }

  body {
    background: ${props => props.theme.colors.background};
    color: ${props => props.theme.colors.text};
    font: 400 16px Roboto, sans-serif;
  }
  h2{
    color: #000;
    font-style: normal;
    font-weight: bold;
    font-size: 16px;
    line-height: 36px;
    letter-spacing: 0.08em;

  }

`;
