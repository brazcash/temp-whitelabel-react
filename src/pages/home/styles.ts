import styled from 'styled-components'
import Image from 'next/image'

export const BannerContainer = styled.div`  
  border: 1px solid red;
  background-size: contain;
  background-image: url(${(props)=>props.imgUrl});
  background-repeat: no-repeat;
  height: 1140px;
  max-width: 1920px;
  margin: 0 auto;
`

export const TextBannerContainer = styled.div`
  padding-left: 91px;
  
`

export const TextBannerPartOne = styled.p`

`

export const TextBannerPartTwo = styled.p`

`
