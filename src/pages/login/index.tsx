import React from 'react'
import Head from 'next/head'

import Card from '../../components/Card'
import Button from '../../components/Button'
import Input from '../../components/Input'
import { ContainerMain, InputsContainer } from '../register/styles'

const Login: React.FC = () => {
  return (
    <div>
      <Head>
        <title>Cashbanx</title>
      </Head>

      <main>
        <ContainerMain>
          <Card>
            <h2>ACESSAR COM</h2>
            <InputsContainer>
              <Button className="sm">CPF</Button>
              <Button className="light sm">E-MAIL</Button>
            </InputsContainer>
            <Input placeholder="Digite seu E-mail" />
            <Input placeholder="Digite sua senha" type="password" />
            <Button className="md">ACESSAR</Button>
          </Card>
        </ContainerMain>
      </main>

    </div>
  )
}

export default Login
