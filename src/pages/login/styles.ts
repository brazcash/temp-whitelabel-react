import styled from 'styled-components';

export const ContainerMain = styled.div`
  background-color: #FEFEFE;
  display: flex;
  justify-content: center;
`

export const InputsContainer = styled.div`
  display: flex;
  justify-content: space-between;
`;
