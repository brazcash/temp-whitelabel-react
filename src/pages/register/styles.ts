import styled from 'styled-components'

export const ContainerMain = styled.div`
  align-items: center;
  background-color: #FEFEFE;
  display: flex;
  flex-direction: column;
  justify-content: center;
`

export const ContainerImgageLogo = styled.div`
  margin-top: 174px;
`
export const ContainerForm = styled.div`
  background-color: #F7F7F7;
  border-radius: 17px;
  margin: 23px 0;
  padding: 25px 50px;
  width: 633px;
`;

export const TitleForm = styled.h2`
  color: black;
  font-size: 20px;
  font-weight: 700;
  line-height: 36px;
  margin: 14px 0;
  text-align: center;
  text-transform: uppercase;
`

export const InputsContainer = styled.div`
  display: flex;
  justify-content: space-between;
  & > input {
    max-width: 253px;
  }
`

export const ButtonRegisterContainer = styled.div`
  display: flex;
  justify-content: center;
  margin: 18px 0;
`

export const AlreadyAccountContainer = styled.div`
  display: flex;
  margin-bottom: 241px;
  & a {
  color: #1449d3;
  font-weight: 600;
  text-decoration: none;
  }
`

export const AlreadyAccountText = styled.p`
  color: black;
  font-weight: 600;
  margin-right: 7px;
`
