import React from 'react'
import Head from 'next/head'
import Image from 'next/image'
import Link from 'next/link'
import Input from '../../components/Input'
import Button from '../../components/Button'
import {
  ContainerMain,
  ContainerImgageLogo,
  ContainerForm,
  TitleForm,
  InputsContainer,
  ButtonRegisterContainer, AlreadyAccountContainer,
  AlreadyAccountText,
} from './styles'
type Props = {

};

const Register: React.FC = (props: Props) => {
  return (
    <div>
      <Head>
        <title>Cashbanx</title>
      </Head>

      <main>
        <ContainerMain>
          <ContainerImgageLogo>
            <Image
              src="/img/cashbanx-logo.png"
              alt="logo cashbanx"
              width={565}
              height={96}
            />
          </ContainerImgageLogo>
          <ContainerForm>
            <TitleForm>
              cadastre-se
            </TitleForm>
            <Input placeholder="Nome Completo" />
            <Input placeholder="CPF" />
            <Input placeholder="Email" />
            <Input placeholder="Telefone" />
            <Input placeholder="Senha" />
            <Input placeholder="Confirmar senha" />
            <TitleForm>
              para receber o cashback
            </TitleForm>
            <Input placeholder="Banco (deve aceitar PIX)" />
            <InputsContainer>
              <Input
                placeholder="Agência"
              />
              <Input
                placeholder="Conta Corrente"
              />
            </InputsContainer>
            <ButtonRegisterContainer>
              <Button className="md">
                cadastrar
              </Button>
            </ButtonRegisterContainer>
          </ContainerForm>
          <AlreadyAccountContainer>
            <AlreadyAccountText>
              Já tem conta?
            </AlreadyAccountText>
            <Link href="/login">Acesse aqui!</Link>
          </AlreadyAccountContainer>
        </ContainerMain>
      </main>

    </div>
  )
}

export default Register
