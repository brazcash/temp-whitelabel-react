import React from 'react'
import Image from 'next/image'
import Head from 'next/head'
import Header from '../components/Header';
import {
  BannerContainer,
  TextBannerContainer,
  TextBannerPartOne,
  TextBannerPartTwo,
} from './home/styles'


const Home: React.FC = () => {
  return (
    <div>
      <Head>
        <title>Cashbanx</title>
      </Head>

      <main>
        <Header />
        <BannerContainer imgUrl={"/img/banner-home.png"}>
          <TextBannerContainer>
            <Image
              src="/img/cashbanx-logo.png"
              alt="logo cashbanx"
              width={676}
              height={141}
              objectPosition={-28}
            />
            <TextBannerPartOne>
              Ofereça dinheiro de volta em todas as compras online dos seus clientes!
            </TextBannerPartOne>
            <TextBannerPartTwo>
              Marketplace completo para Bancos, Empresas e Clubes de Futebol.
            </TextBannerPartTwo>
          </TextBannerContainer>
        </BannerContainer>
      </main>
    </div>
  )
}

export default Home
